#include <vector>
#include <cmath>
#include <algorithm>
#include <iostream>
#include "tgaimage.h"
#include "model.h"
#include "geometry.h"


const TGAColor white = TGAColor(255, 255, 255, 255);
const TGAColor red = TGAColor(255, 0, 0, 255);
const TGAColor green = TGAColor(0, 255, 0, 255);
Model* model = nullptr;

const int width = 800;
const int height = 800;


void line(int x0, int y0, int x1, int y1, TGAImage& image, TGAColor color);

void triangle(Vec2i t0, Vec2i t1, Vec2i t2, TGAImage& image, TGAColor color);

int main(int argc, char** argv) {
	if (2 == argc) {
		model = new Model(argv[1]);
	}
	else {
		model = new Model("obj/african_head.obj");
	}

	TGAImage image(width, height, TGAImage::RGB);

	Vec3f light_dir(0, 0, -1);
	for (int i = 0; i < model->nfaces(); i++) {
		std::vector<int> face = model->face(i);
		Vec2i screen_coords[3];
		Vec3f world_coords[3];
		for (int j = 0; j < 3; j++) {
			Vec3f v = model->vert(face[j]);
			screen_coords[j] = Vec2i((v.x + 1.) * width / 2., (v.y + 1.) * height / 2.);
			world_coords[j] = v;
		}
		Vec3f n = (world_coords[2] - world_coords[0]) ^ (world_coords[1] - world_coords[0]);
		n.normalize();
		float intensity = n * light_dir;
		if (intensity > 0) {
			triangle(screen_coords[0], screen_coords[1], screen_coords[2], image, TGAColor(intensity * 255, intensity * 255, intensity * 255, 255));
		}
	}
	
	//Vec2i t0(472, 345);
	//Vec2i t1(473, 340);
	//Vec2i t2(481, 338);

	//triangle(t2, t0, t1, image, green);

	image.flip_vertically(); // i want to have the origin at the left bottom corner of the image
	image.write_tga_file("output.tga");
	delete model;
	return 0;
}


void line(int x0, int y0, int x1, int y1, TGAImage& image, TGAColor color)
{
	bool steep = false;
	if (std::abs(x0 - x1) < std::abs(y0 - y1))
	{
		std::swap(x0, y0);
		std::swap(y1, x1);
		steep = true;
	}
	if (x0 > x1)
	{
		std::swap(x0, x1);
		std::swap(y0, y1);
	}
	int dx = x1 - x0;
	int dy = y1 - y0;
	int derror2 = std::abs(dy) * 2;  // ����������, ����� ���������� �� float � �������.
	int error2 = 0;
	int y = y0;
	for (int x = x0; x < x1; x++)
	{
		if (steep)
		{
			image.set(y, x, color);
		}
		else
		{
			image.set(x, y, color);
		}
		error2 += derror2;

		if (error2 > dx)
		{
			y += (y1 > y0 ? 1 : -1);
			error2 -= dx * 2;
		}
	}
}

void triangle(Vec2i t0, Vec2i t1, Vec2i t2, TGAImage& image, TGAColor color) {
	if (t0.x > t1.x) std::swap(t0, t1);
	if (t1.x > t2.x) std::swap(t1, t2);
	if (t0.x > t1.x) std::swap(t0, t1);

	int skewVectorProduct = (t1.y - t0.y) * (t2.x - t0.x) - (t2.y - t0.y) * (t1.x - t0.x);  // ����� ��������� ������������ ��� ����������� � ����� ����������� �������� ������������ �����.
	if (skewVectorProduct == 0) return;
	int stepY = skewVectorProduct > 0 ? -1 : 1;

	float middle_alpha;
	float middle_beta;
	float extreme_alpha;
	float extreme_beta;

	// ������� ���� � b ��� ����� ��������� � ����������� ������ ������������.
	if (t1.x == t0.x) {
		middle_alpha = 0;
		middle_beta = t1.y;
	}
	else {
		middle_alpha = (float)(t1.y - t0.y) / (t1.x - t0.x);  // �������.
		middle_beta = t1.y - (t1.x * middle_alpha);
	}

	// ������� ���� � b ��� ����� ��������� � �������� ������� ������������.
	if (t2.x == t0.x) {
		extreme_alpha = 0;
		extreme_beta = t2.y;
	}
	else {
		extreme_alpha = (float)(t2.y - t0.y) / (t2.x - t0.x);
		extreme_beta = t2.y - (t2.x * extreme_alpha);
	}

	// ������ �������� �� t0 � t1 , ������ �� t1 � t2. 
	for (int i = 0; i < 2; i++) {

		for (int x = t0.x; x < t1.x; x++) {
			int middleY = x * middle_alpha + middle_beta;  // y = kx + b
			int extremeY = x * extreme_alpha + extreme_beta + stepY; // stepY ����� ���� ��������� ������ ������� extremY.
			int debug = 0;

			// �������� ������������ �����, ����� ��������� �����������.
			if (stepY == 1) {
				while (middleY < extremeY) {
					image.set(x, middleY, color);
					middleY += stepY;
				}
			}
			else {
				while (middleY > extremeY) {
					image.set(x, middleY, color);
					middleY += stepY;
				}
			}
		}

		if (t1.x == t2.x) {
			break;
		}

		// ������ ��������� ��� ������ �����.
		middle_alpha = (float)(t2.y - t1.y) / (t2.x - t1.x);
		middle_beta = t2.y - (t2.x * middle_alpha);;
		std::swap(t0, t1);
		std::swap(t1, t2);
	}
}