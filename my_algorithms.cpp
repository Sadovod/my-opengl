#include <vector>
#include <cmath>
#include "tgaimage.h"
#include "model.h"
#include "geometry.h"

// ���������� �������������� ���������� ��� �������������
std::vector<std::vector<int>> my_rect(std::vector<std::vector<int>> triangle)
{
	int max_x = triangle[0][0];
	int min_x = max_x;
	int max_y = triangle[0][1];
	int min_y = max_y;

	for (int i = 1; i < 3; i++)
	{
		max_x = (triangle[i][0] > max_x) ? triangle[i][0] : max_x;
		min_x = (triangle[i][0] < min_x) ? triangle[i][0] : min_x;
		max_y = (triangle[i][1] > max_y) ? triangle[i][1] : max_y;
		min_y = (triangle[i][1] < min_y) ? triangle[i][1] : min_y;
	}

	std::vector<std::vector<int>> rect{ {min_x, min_y}, {max_x, min_y}, {max_x, max_y}, {min_x, max_y} };

	return rect;
}